#include "DZ31.5.h"
//#include <thread>
#include <chrono>
#include <unordered_set>

#define DBG(code) code

//��������������� ����!!!!!!!
class IGraph {
	std::vector <int> vertices;
public:
	virtual ~IGraph() {}
	IGraph() {};
	IGraph(IGraph *_oth) {
		int othSize = _oth->VerticesCount();
		std::vector <int> nextVert;

		for(int i = 0; i < othSize; i++){
			_oth->GetNextVertices(i, nextVert);
			for (auto it : nextVert) this->AddEdge(i, it);
			nextVert.clear();
		}
	};
	virtual void AddEdge(int from, int to) = 0; // ����� ��������� ������� ������ � ����� ����� � ��������� �����
	virtual int VerticesCount() const = 0; // ����� ������ ������� ������� ���������� ������
	virtual void GetNextVertices(int vertex, std::vector<int> &vertices) const = 0; // ��� ���������� ������� ����� ������� � ������ ��������� ��� �������, � ������� ����� ����� �� ����� �� ������
	virtual void GetPrevVertices(int vertex, std::vector<int> &vertices) const = 0; // ��� ���������� ������� ����� ������� � ������ ��������� ��� �������, �� ������� ����� ����� �� ����� � ������
};

class MatrixGraph : public IGraph {
	std::vector <std::vector <bool>> matrix;
	int maxKnot = 0;
public:
	MatrixGraph() {};
	MatrixGraph(IGraph *_oth) /*: IGraph::IGraph(_oth) {};*/{
		int othSize = _oth->VerticesCount();
		std::vector <int> nextVert;

		for (int i = 1; i <= othSize; i++) {
			_oth->GetNextVertices(i, nextVert);
			for (auto it : nextVert) this->AddEdge(i, it);
			nextVert.clear();
		}
	};
	void AddEdge(int from, int to) {
		int maxSize = (from > to) ? from : to;

		if (from <= 0 || to <= 0) { 
			std::cout << "Incorrect number Vertices (must > 0)" << std::endl;
			return;
		}

		if (maxKnot < maxSize) matrix.resize(maxSize);

		for (auto &it : matrix) 
			if (it.size() < maxSize) 
				it.resize(maxSize);
		
		matrix[from - 1][to - 1] = true;

		maxKnot = maxSize;
	}
	
	int VerticesCount() const {
		return maxKnot;
	}

	void GetNextVertices(int vertex, std::vector<int> &vertices) const {
		
		if (vertex > maxKnot) {
			std::cout << "No vertex number " << vertex << " , max number" << maxKnot << std::endl;
			return;
		}
		std::cout << "NextVertices" << std::endl;
		for (int i = 0; i < maxKnot; i++) { 
			if (matrix[vertex - 1][i]) { 
				DBG(std::cout << i + 1 << std::endl);
				vertices.push_back(i + 1);
			}
		}
	}
	void GetPrevVertices(int vertex, std::vector<int> &vertices) const {
		if (vertex > maxKnot) {
			std::cout << "No vertex number " << vertex << " , max number" << maxKnot << std::endl;
			return;
		}
		std::cout << "PrevVertices" << std::endl;
		for (int i = 0; i < maxKnot; i++)
		{
			if (matrix[i][vertex - 1]) { 
				DBG(std::cout << i + 1 << std::endl);
				vertices.push_back(i + 1);
			}
		}
	}
};

/*struct Knot {
	int num;
	Knot* next;
};*/

class ListGraph : public IGraph {
	//������� � ���� ������� ������� ���������
	//std::vector<Knot> knots;
	std::vector<std::unordered_set<int>> knots;
public:
	ListGraph() {};
	ListGraph(IGraph *_oth) {
		int othSize = _oth->VerticesCount();
		std::vector <int> nextVert;

		for (int i = 1; i <= othSize; i++) {
			_oth->GetNextVertices(i, nextVert);
			for (auto it : nextVert) this->AddEdge(i, it);
			nextVert.clear();
		}
	}
	void AddEdge(int from, int to) {
		int maxSize = (from > to) ? from : to;

		if (knots.size() < maxSize) knots.resize(maxSize);
		knots[from - 1].insert(to);
	}

	int VerticesCount() const {
		return knots.size();
	}

	void GetNextVertices(int vertex, std::vector<int> &vertices) const{
		std::cout << "Print Next vertices" << std::endl;
		for (auto it : knots[vertex - 1]) { 
			DBG(std::cout << it << std::endl);
			vertices.push_back(it);
		}
	}
	void GetPrevVertices(int vertex, std::vector<int> &vertices) const {
		for (int i = 0; i < knots.size(); i++) {
			if (knots[i].find(vertex) != knots[i].end()) { 
				DBG(std::cout << i + 1 << std::endl);
				vertices.push_back(i + 1);
			}
			//knots[i].find(vertex);
		}
	}
	~ListGraph() {
/*		Knot *next, *cur;

		for (auto it : knots) {
			for (cur = it.next; cur; cur = next) { 
				if (cur->next) next = cur->next; 
				delete cur;
			}
		}*/
	}
};

void task2() {
	int sDot, eDot;
	IGraph *nGraph = new ListGraph;
	IGraph *cpGraph = nullptr;
	std::vector<int> vec;

	while (1) {
		std::cout << "Enter start dot num or 0 for exit or -1 for go to get info" << std::endl;
		std::cin >> sDot;
		if (sDot == 0) break;
		else if (sDot > 0) {
			std::cout << "Enter end dot num" << std::endl;
			std::cin >> eDot;
			nGraph->AddEdge(sDot, eDot);
			continue;
		}
		if (!sDot) continue;
		std::cout << "Enter vertice num" << std::endl;
		std::cin >> sDot;
		nGraph->GetNextVertices(sDot, vec);
		//if(!cpGraph) cpGraph = new MatrixGraph(nGraph);
		IGraph *cpGraph = new MatrixGraph(nGraph);
		std::cout << "From Other matrix" << std::endl;
		cpGraph->GetNextVertices(sDot, vec);
	}
	//std::this_thread::sleep_for(std::chrono::seconds(2));
}