#include "DZ31.5.h"

//����� ����� �������
class Toy {
	std::string name;
public:
	Toy() : name("SomeToy") {}
	Toy(std::string _name) : name(_name) {};
};
//����� ������ ��������� �� �������
class SharedToyPtr {
	Toy* obj;
	int *counter;
public:
	//�����������(�� �����)
	SharedToyPtr(std::string name) {
		obj = new Toy(name);
		counter = new int(1);
		//*counter = 1;
	}
	//�����������(��� �����)
	SharedToyPtr() : SharedToyPtr("Sometoy") {};
	
	//���������� �����������
	SharedToyPtr(SharedToyPtr &oth) : obj(oth.obj) {
		*(oth.counter) += 1;
		counter = oth.counter;
	}
	//�������� ������������
	SharedToyPtr& operator=(SharedToyPtr &oth) {
		if (this == &oth) return;
		
		*(oth.counter) += 1;
		if (*counter > 0) *counter--;
		if (*counter <= 0 && obj) delete obj;
		obj = oth.obj;

		return oth;
	}
	//����������
	~SharedToyPtr() {
		if (!obj) return;
		*(counter) -= 1;
		if (*counter == 0 && obj) delete obj;
	}
	void reset() {
		*(counter) -= 1;
		if (!counter) delete obj;
		obj = nullptr;
		counter = nullptr;
	};
};

//�������� ����� ������� -> ������ ������
SharedToyPtr makeToyPtr(std::string name) {
	SharedToyPtr toy(name);

	return toy;
}
//����������� ������� -> ������ ����������� �������
SharedToyPtr makeToyPtr(SharedToyPtr& othToy) {
	SharedToyPtr toy(othToy);

	return toy;
}

class Dog{
    std::string name;
	//std::shared_ptr<Toy> lovelyToy;
	SharedToyPtr lovelyToy;
public:
	//Dog(std::string newName, std::shared_ptr<Toy> toy) : name(newName), lovelyToy(toy) {};
	Dog(std::string newName, SharedToyPtr toy) : name(newName), lovelyToy(toy) {};
	Dog(std::string newName): name(newName){}
	Dog() : Dog("SomeDog") {};


	//void chengeToy(std::shared_ptr<Toy>& newToy) {
	void chengeToy(SharedToyPtr& newToy) {
		lovelyToy = newToy;
	}
};

void task1(){
	SharedToyPtr a("bond");
	Dog* d = new Dog("Sharik", a);
	//std::shared_ptr<Dog> sh = std::make_shared<Dog>("Vasya", a);

	a.reset();
	//sh.reset();
	//std::cout << "Hello 1" << std::endl;

	delete d;

}